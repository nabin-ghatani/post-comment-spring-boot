package com.example.model;

public class LikeUnlikeDto {
    private Short likeCount;
    private int unlikeCount;
    private int postId;
    private short userAction;

    public short getUserAction() {
        return userAction;
    }

    public void setUserAction(short userAction) {
        this.userAction = userAction;
    }

    public Short getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Short likeCount) {
        this.likeCount = likeCount;
    }

    public int getUnlikeCount() {
        return unlikeCount;
    }

    public void setUnlikeCount(int unlikeCount) {
        this.unlikeCount = unlikeCount;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
