package com.example.model;

import com.example.entity.Comment;
import com.example.entity.LikeOrUnlike;
import com.example.entity.Post;
import com.example.entity.User;

public class ApplicationMessage {
    private boolean success;
    private Post post;
    private Comment comment;
    private String message;
    private LikeOrUnlike likeOrUnlike;
    private User user;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LikeOrUnlike getLikeOrUnlike() {
        return likeOrUnlike;
    }

    public void setLikeOrUnlike(LikeOrUnlike likeOrUnlike) {
        this.likeOrUnlike = likeOrUnlike;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
