package com.example.config;

import com.example.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder encoder;
    @Autowired
    private UserLoginService userLoginService;
    @Autowired
    private SecurityHandler securityHandler;

    /*
     * authentication types
     * 1. inMemory authentication :: hardcode => username , password
     * 2. jdbc auth
     * 3. jpa/userDetails service auth
     * 4. ldap
     * 5. oauth
     * 6. jwt
     */

    // user location , userName , userPassword , user details
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("nabin@gmail.com").password(encoder.encode("nabin")).roles("USER");
//        auth.inMemoryAuthentication().withUser("admin@gmail.com").password(encoder.encode("admin")).roles("ADMIN");
        auth.userDetailsService(userLoginService).passwordEncoder(encoder);
    }

    //http path => control controller paths...
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/post/create-post").hasAnyRole("ADMIN,USER")
                .antMatchers("/post/*/comments").hasAnyRole("ADMIN","USER")
                .antMatchers("/post/like-unlike/**").permitAll()
                .antMatchers("/").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .passwordParameter("password")
                .failureUrl("/login?error")
                .successHandler(securityHandler)
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .and()
                .csrf().disable();
    }


    /*
    without bean, we should have created a new object of BCryptPasswordEncoder everytime
    when we have to access BCryptPasswordEncoder methods which is costly.
    With a bean, we give this responsibility of creating new object & managing it to spring
    and we can accomplish it by autowiring  BCryptPasswordEncoder*/

    @Bean // IOC : object
    public BCryptPasswordEncoder getBCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
