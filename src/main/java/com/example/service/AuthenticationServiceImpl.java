package com.example.service;

import com.example.entity.CurrentlyLoggedInUser;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationServiceImpl implements AuthenticationService{

    @Override
    public int getCurrentlyLoggedInUserId(Authentication authentication) {
        int loggedInUserId = 0;
        if (authentication != null) {
            CurrentlyLoggedInUser loggedInUser = (CurrentlyLoggedInUser) authentication.getPrincipal();
            loggedInUserId = loggedInUser.getUserId();
        }
        return loggedInUserId;
    }
}
