package com.example.service;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public interface AuthenticationService {
    int getCurrentlyLoggedInUserId(Authentication authentication);
}
