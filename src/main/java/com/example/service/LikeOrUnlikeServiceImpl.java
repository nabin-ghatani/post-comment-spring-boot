package com.example.service;

import com.example.entity.LikeOrUnlike;
import com.example.model.LikeUnlikeDto;
import com.example.repo.LikeOrUnlikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public class LikeOrUnlikeServiceImpl implements LikeOrUnlikeService {

    @Autowired
    private LikeOrUnlikeRepository likeOrUnlikeRepository;
    @Autowired
    private AuthenticationService authenticationService;

    @Override
    public LikeUnlikeDto getLikeUnlike(Authentication authentication,int postId) {
        Short likeCount = likeOrUnlikeRepository.selectLikeCountInPost(postId);
        Integer unlikeCount = likeOrUnlikeRepository.selectUnLikeCountInPost(postId);

        LikeUnlikeDto likeUnlikeDto = new LikeUnlikeDto();
        likeUnlikeDto.setLikeCount(likeCount == null ? 0 : likeCount);
        likeUnlikeDto.setUnlikeCount(unlikeCount == null ? 0 : unlikeCount);

        if(authentication != null){
            LikeOrUnlike likeOrUnlike = likeOrUnlikeRepository.findByUserIdAndPostId(authenticationService.getCurrentlyLoggedInUserId(authentication),postId);
            if(likeOrUnlike!=null)
                likeUnlikeDto.setUserAction(likeOrUnlike.getLikeCount());
        }

        return likeUnlikeDto;
    }

    @Override
    public void setLikeUnlikeInPost(Authentication authentication, int postId, short likeCount) {
        LikeOrUnlike likeOrUnlike = likeOrUnlikeRepository.findByUserIdAndPostId(authenticationService.getCurrentlyLoggedInUserId(authentication), postId);
        if (likeOrUnlike == null) {
            likeOrUnlike = new LikeOrUnlike();
            likeOrUnlike.setUserId(authenticationService.getCurrentlyLoggedInUserId(authentication));
            likeOrUnlike.setPostId(postId);
        }
        likeOrUnlike.setLikeCount(likeCount);
        likeOrUnlikeRepository.save(likeOrUnlike);
    }

}
