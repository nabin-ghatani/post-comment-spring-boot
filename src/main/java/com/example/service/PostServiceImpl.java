package com.example.service;

import com.example.entity.Comment;
import com.example.entity.CurrentlyLoggedInUser;
import com.example.entity.Post;
import com.example.entity.User;
import com.example.repo.CommentRepository;
import com.example.repo.PostRepository;
import com.example.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void createPost(Post post, Authentication authentication) {
        CurrentlyLoggedInUser loggedInUser=(CurrentlyLoggedInUser) authentication.getPrincipal();
        post.setUser(userRepository.getOne(loggedInUser.getUserId()));
        postRepository.save(post);
    }

    @Override
    public List<Post> getAllPost() {
        return postRepository.findAll();
    }

    @Override
    public List<Post> getAllPostOfUserById(int userId) {
        User user = userRepository.findById(userId).get();
        return postRepository.findAllByUser(user);
    }

    @Override
    public Post getPostById(int id) {
        return postRepository.findById(id).get();
    }

    @Override
    public void deletePostByPostId(int id) {
        Post post = postRepository.findById(id).get();
        deleteCommentsOfPost(id);
        postRepository.delete(post);
    }

    @Override
    public List<Post> getAllPostsAtDate(String date) {
        //date = 2021-03-07 11:46:00.002000
        //datePartOnly[0] = 2021-03-07
        String[] datePartOnly = date.split("\\s+");
        return postRepository.findAllByCreatedAt(datePartOnly[0]);
    }

    private void deleteCommentsOfPost(int id) {
        Post post = postRepository.findById(id).get();
        List<Comment> commentList = post.getComments();
        commentRepository.deleteAll(commentList);
    }
}
