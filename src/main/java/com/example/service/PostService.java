package com.example.service;

import com.example.entity.Post;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostService {
    void createPost(Post post, Authentication authentication);
    List<Post> getAllPost();
    List<Post> getAllPostOfUserById(int userId);
    Post getPostById(int id);
    void deletePostByPostId(int id);
    List<Post> getAllPostsAtDate(String date);
}
