package com.example.service;

import com.example.entity.Comment;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CommentService {
    void saveComment(Comment comment, Authentication authentication);
    Comment getCommentById(int id);
    List<Comment> getAllComments();
    void deleteCommentById(int id);
}
