package com.example.service;

import com.example.entity.Comment;
import com.example.entity.CurrentlyLoggedInUser;
import com.example.repo.CommentRepository;
import com.example.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void saveComment(Comment comment, Authentication authentication) {
        CurrentlyLoggedInUser loggedInUser = (CurrentlyLoggedInUser) authentication.getPrincipal();
            comment.setUser(userRepository.findById(loggedInUser.getUserId()).get());
            commentRepository.save(comment);
    }

    @Override
    public Comment getCommentById(int id) {
        return commentRepository.findById(id).get();
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public void deleteCommentById(int id) {
        Comment comment = commentRepository.findById(id).get();
        commentRepository.delete(comment);
    }
}
