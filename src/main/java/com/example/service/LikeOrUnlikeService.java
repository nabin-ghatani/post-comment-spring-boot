package com.example.service;

import com.example.model.LikeUnlikeDto;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

@Service
public interface LikeOrUnlikeService {
    LikeUnlikeDto getLikeUnlike(Authentication authentication,int postId);
    void setLikeUnlikeInPost(Authentication authentication,int postId, short likeCount);
}
