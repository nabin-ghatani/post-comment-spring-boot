package com.example.service;

import com.example.entity.Post;
import com.example.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {
    void createUser(User user);
    User getUserById(int id);
    List<User> getAllUsers();
}
