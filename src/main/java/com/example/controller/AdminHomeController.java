package com.example.controller;

import com.example.service.AuthenticationService;
import com.example.service.PostService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @Autowired
    private UserService userService;
    @Autowired
    private PostService postService;
    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("")
    public ModelAndView goAdminHome(ModelAndView modelAndView, Authentication authentication){
        modelAndView.setViewName("admin-index");
        modelAndView.addObject("users",userService.getAllUsers());
        modelAndView.addObject("loggedInUserId",authenticationService.getCurrentlyLoggedInUserId(authentication));
        return modelAndView;
    }

    @GetMapping("/{userId}/posts")
    public ModelAndView getUserPostsPage(@PathVariable int userId,ModelAndView modelAndView,Authentication authentication){
        modelAndView.setViewName("admin-user-posts");
        modelAndView.addObject("user",userService.getUserById(userId));
        modelAndView.addObject("posts",postService.getAllPostOfUserById(userId));
        modelAndView.addObject("loggedInUserId",authenticationService.getCurrentlyLoggedInUserId(authentication));
        return modelAndView;
    }

    @GetMapping(value = "/user-post/{postId}/delete-post")
    public String deleteUserPost(@PathVariable int postId){
        postService.deletePostByPostId(postId);
        return "redirect:/admin";
    }

    @GetMapping(value = "/posts/{createdDate}")
    public ModelAndView getAllPostsAtCreatedDate(@PathVariable String createdDate, ModelAndView modelAndView, Authentication authentication) {
        modelAndView.setViewName("admin-posts-at-createdDate");
        modelAndView.addObject("posts",postService.getAllPostsAtDate(createdDate));
        modelAndView.addObject("loggedInUserId",authenticationService.getCurrentlyLoggedInUserId(authentication));
        return modelAndView;
    }
}
