package com.example.controller;

import com.example.entity.User;
import com.example.model.ApplicationMessage;
import com.example.service.AuthenticationService;
import com.example.service.PostService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class UserHomeController {

    @Autowired
    private PostService postService;
    @Autowired
    private UserService userService;
    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("")
    public ModelAndView goHome(ModelAndView modelAndView, Authentication authentication) {
        modelAndView.setViewName("index");
        modelAndView.addObject("loggedInUserId", authenticationService.getCurrentlyLoggedInUserId(authentication));
        modelAndView.addObject("posts", postService.getAllPost());
        return modelAndView;
    }

    @GetMapping(value = "login")
    public ModelAndView getLoginPage(ModelAndView modelAndView) {
        modelAndView.setViewName("login");
        modelAndView.addObject("user", new User());
        return modelAndView;
    }

    @PostMapping(value = "register")
    public String processUserRegistration(@ModelAttribute User user) {
        userService.createUser(user);
        return "redirect:/login?success";
    }

    @GetMapping("{userId}")
    @ResponseBody
    public ApplicationMessage fetchUserById(@PathVariable int userId) {
        ApplicationMessage applicationMessage = new ApplicationMessage();
        User user = userService.getUserById(userId);

        if (user == null) {
            applicationMessage.setSuccess(false);
            applicationMessage.setMessage("User not found");
        } else {
            applicationMessage.setSuccess(true);
            applicationMessage.setUser(user);
        }
        return applicationMessage;
    }

    @GetMapping(value = "user/{userId}/profile")
    public ModelAndView getUserProfilePage(@PathVariable int userId, ModelAndView modelAndView, Authentication authentication) {
        int id = 0;
        int loggedInUserId = authenticationService.getCurrentlyLoggedInUserId(authentication);
        if (userId == loggedInUserId) {
            if (authentication != null) {
                id = loggedInUserId;
            }
        } else
            id = userService.getUserById(userId).getUserId();

        if (authentication != null)
            modelAndView.addObject("role", userService.getUserById(loggedInUserId).getRole());

        modelAndView.setViewName("user-profile");
        modelAndView.addObject("userId", id);
        return modelAndView;
    }
}
