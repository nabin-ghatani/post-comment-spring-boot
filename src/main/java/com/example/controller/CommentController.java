package com.example.controller;

import com.example.entity.Comment;
import com.example.service.CommentService;
import com.example.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/post")
public class CommentController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private PostService postService;

    @PostMapping("/{id}/comments")
    public String processPostComments(@ModelAttribute Comment comment, @PathVariable int id, Authentication authentication) {
        if (authentication != null) {
            comment.setPost(postService.getPostById(id));
            commentService.saveComment(comment, authentication);
            return "redirect:/post/" + id;
        }
        return "redirect:/";
    }

    @GetMapping(value = "/{postId}/{commentId}/delete-comment")
    public String deletePostComment(@PathVariable int postId,@PathVariable int commentId) {
        commentService.deleteCommentById(commentId);
        return "redirect:/post/"+postId;
    }
}