package com.example.controller;

import com.example.entity.Comment;
import com.example.entity.Post;
import com.example.service.AuthenticationService;
import com.example.service.PostService;
import com.example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;
    @Autowired
    private AuthenticationService authenticationService;
    @Autowired
    private UserService userService;

    @GetMapping(value = "create-post")
    public ModelAndView getCreatePostPage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("create-post");
        modelAndView.addObject("post", new Post());
        return modelAndView;
    }

    @PostMapping(value = "/create-post")
    public String getCreatePostPage(@ModelAttribute @Valid Post post, BindingResult bindingResult, Authentication authentication) {
        if (bindingResult.hasErrors())
            return "create-post";
        postService.createPost(post,authentication);
        return "redirect:/";
    }

    @GetMapping(value = "/{id}")
    public ModelAndView getViewPostDetailsPage(@PathVariable int id,ModelAndView modelAndView,Authentication authentication) {
        modelAndView.setViewName("view-post-details");
            modelAndView.addObject("post", postService.getPostById(id));
            modelAndView.addObject("comment", new Comment());
            if(authentication != null) {
                modelAndView.addObject("loggedInUserId", authenticationService.getCurrentlyLoggedInUserId(authentication));
                modelAndView.addObject("role", userService.getUserById(authenticationService.getCurrentlyLoggedInUserId(authentication)).getRole());
            }
        return modelAndView;
    }

    @GetMapping(value = "/delete-post/{id}")
    public String deletePostById(@PathVariable int id) {
        postService.deletePostByPostId(id);
        return "redirect:/";
    }

    @GetMapping(value = "/edit-post/{id}")
    public ModelAndView editPostById(@PathVariable int id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("create-post");
        modelAndView.addObject("post", postService.getPostById(id));
        return modelAndView;
    }
}
