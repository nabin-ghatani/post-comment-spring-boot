package com.example.controller;

import com.example.model.ApplicationMessage;
import com.example.model.LikeUnlikeDto;
import com.example.service.LikeOrUnlikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/post/like-unlike")
public class LikeUnlikeController {

    @Autowired
    private LikeOrUnlikeService likeOrUnlikeService;

    @GetMapping(value = "/{postId}")
    public LikeUnlikeDto fetchLikeUnlikeInPost(@PathVariable int postId,Authentication authentication) {
        return likeOrUnlikeService.getLikeUnlike(authentication,postId);
    }

    @PostMapping(value = "/{postId}")
    public ApplicationMessage processLikeUnlikeInPost(@PathVariable int postId,
                                                      Authentication authentication,
                                                      @RequestBody HashMap<String, Short> map) {
        ApplicationMessage applicationMessage = new ApplicationMessage();
        if (authentication == null) {
            applicationMessage.setSuccess(false);
            applicationMessage.setMessage("Not authenticated user");
        } else {
            Short likeCount = map.get("likeCount");
            likeOrUnlikeService.setLikeUnlikeInPost(authentication, postId, likeCount);
            applicationMessage.setSuccess(true);
        }
        return applicationMessage;
    }
}
