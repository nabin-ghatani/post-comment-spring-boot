package com.example.repo;

import com.example.entity.Post;
import com.example.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {
    List<Post> findAllByUser(User user);

    @Query(value = "select *from tbl_posts t where t.created_at like %?1%",nativeQuery = true)
    List<Post> findAllByCreatedAt(String createdAt);
}