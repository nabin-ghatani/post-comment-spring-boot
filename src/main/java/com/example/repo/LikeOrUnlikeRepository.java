package com.example.repo;

import com.example.entity.LikeOrUnlike;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeOrUnlikeRepository extends JpaRepository<LikeOrUnlike, Integer> {
    /**
     * native query
     *
     * @Query(value = "SELECT COUNT (like_count) FROM tbl_like_unlike tlu WHERE tlu.like_count = 1 " +
     * "GROUP BY tlu.fk_post_id HAVING tlu.fk_post_id= 102", nativeQuery = true)
     */

    //jpa query
    @Query(value = "SELECT COUNT (tlu.likeCount) FROM LikeOrUnlike tlu WHERE tlu.likeCount = 1 " +
            "GROUP BY tlu.postId HAVING tlu.postId= ?1")
    Short selectLikeCountInPost(int postId);


    @Query(value = "SELECT COUNT (tlu.likeCount) FROM LikeOrUnlike tlu WHERE tlu.likeCount = -1 " +
            "GROUP BY tlu.postId HAVING tlu.postId= ?1")
    Integer selectUnLikeCountInPost(int postId);

    //jpa query
    @Query(value = "SELECT COUNT (tlu.likeCount) FROM LikeOrUnlike tlu WHERE tlu.likeCount = ?1 " +
            "GROUP BY tlu.postId HAVING tlu.postId= ?2")
    Integer selectLikeUnlikeCountInPost(int likeOrUnlike, int postId);

    // select * from tbl_like_unlike where fk_user_id=?1 and fk_post_id=?2
    LikeOrUnlike findByUserIdAndPostId(int userId, int postId);
}
