package com.example.entity;

import javax.persistence.*;

@Entity
@Table(name = "tbl_like_unlike")
public class LikeOrUnlike {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "like_count")
    private short likeCount;

    @Column(name = "fk_user_id")
    private int userId;

    @Column(name = "fk_post_id")
    private int postId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(short likeCount) {
        this.likeCount = likeCount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
